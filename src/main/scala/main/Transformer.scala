package fast

import cats._, implicits._
import atto._, Atto._

object Transformer {
    val parse = (s: String) => ExpressionParser.expression.parseOnly(s).option.get
    // https://www.brics.dk/RS/02/2/index.html
    def cpsTransform(e: Expression): Expression = {
        def cDExpr(e: Expression, k: Expression): Expression = {
            e match {
                case ListExp(StrExp("S"), _) =>
                parse(s"(A ${k.show} ${cDTriv(e).show})")
                case ListExp(StrExp("F"), _, _) =>
                parse(s"(A ${k.show} ${cDTriv(e).show}")
                case ListExp(StrExp("A"), _, _) =>
                cDComp(e, k)
            }
        }
        def cDTriv(t: Expression): Expression = {
            t match {
                case ListExp(StrExp("S"), x) => t
                case ListExp(StrExp("F"), x, e) =>
                parse(s"(F ${x.show} (F _k ${cDExpr(e, parse("(S _k)")).show}))")
            }
        }
        def cDComp(s: Expression, c: Expression): Expression = {
            s match {
                case ListExp(StrExp("A"), x1, x2) =>
                (x1, x2) match {
                    case (ListExp(StrExp("A"), _, _), ListExp(StrExp("A"), _, _)) =>
                    cDComp(x1, parse(s"(F _n (A ${cDComp(x2, parse(s"(F _m (A (A _n _m) ${c.show}))")).show}))"))
                    case (ListExp(StrExp("A"), _, _), ListExp(StrExp("F"), _, _)) | (ListExp(StrExp("A"), _, _), ListExp(StrExp("S"), _)) =>
                    cDComp(x1, parse(s"(F _n (A (A (S _n) ${cDTriv(x2).show}) ${c.show}))"))
                    case (ListExp(StrExp("F"), _, _), ListExp(StrExp("A"), _, _)) | (ListExp(StrExp("S"), _), ListExp(StrExp("A"), _, _)) =>
                    cDComp(x2, parse(s"(F _n (A (A ${cDTriv(x1).show} (S _n)) ${c.show}))"))
                    case (ListExp(StrExp("F"), _, _), ListExp(StrExp("F"), _, _)) | (ListExp(StrExp("S"), _), ListExp(StrExp("S"), _)) | (ListExp(StrExp("F"), _, _), ListExp(StrExp("S"), _)) | (ListExp(StrExp("S"), _), ListExp(StrExp("F"), _, _))  =>
                    parse(s"(A (A ${cDTriv(x1).show} ${cDTriv(x2).show}) ${c.show})")
                }
            }
        }
        parse(s"(F _k ${cDExpr(e, parse(s"(S _k)")).show})")
    }
    def dsTransform(e: Expression): Expression = {
        def dCExpr(e: Expression, s: List[Expression]): Expression = {
            e match {
                case ListExp(StrExp("A"), r, ListExp(StrExp("S"), StrExp(k))) if k.charAt(0) == '_'  =>
                dCCont(parse(s"(S ${k.show})"), dCRoot(r, s))
                case ListExp(StrExp("A"), r, ListExp(StrExp("F"), StrExp(n), e)) if n.charAt(0) == '_'  =>
                dCCont(parse(s"(F ${n.show} ${e.show})"), dCRoot(r, s))
                case ListExp(StrExp("A"), ListExp(StrExp("S"), StrExp(k)), t) if k.charAt(0) == '_'  =>
                dCCont(parse(s"(S ${k.show})"), dCTriv(t, s))
                case ListExp(StrExp("A"), ListExp(StrExp("F"), StrExp(n), e), t) if n.charAt(0) == '_'  =>
                dCCont(parse(s"(F ${n.show} ${e.show})"), dCTriv(t, s))
            }
        }
        def dCTriv(t: Expression, s: List[Expression]): (Expression, List[Expression]) = {
            t match {
                case ListExp(StrExp("S"), StrExp(n)) if n.charAt(0) == '_' => (s.head, s.tail)
                case ListExp(StrExp("S"), StrExp(x)) if x.charAt(0) != '_' =>
                (parse(s"(S ${x})"), s)
                case ListExp(StrExp("F"), StrExp(x), ListExp(StrExp("F"), StrExp(k), e)) if k.charAt(0) == '_' =>
                val es = dCRoot(parse(s"(F ${k.show} ${e.show})"), List())
                (parse(s"(F ${x.show} ${es._1.show})"), s)
                case ListExp(StrExp("F"), StrExp(x), ListExp(StrExp("A"), t1, t2)) =>
                val es = dCRoot(parse(s"(A ${t1.show} ${t2.show})"), List())
                (parse(s"(F ${x.show} ${es._1.show})"), s)
            }
        }
        def dCRoot(r: Expression, s: List[Expression]): (Expression, List[Expression]) = {
            r match {
                case ListExp(StrExp("F"), StrExp(k), e) if k.charAt(0) == '_' => (dCExpr(e, List()), s)
                case ListExp(StrExp("A"), t1, t2) => 
                val (e2, s1) = dCTriv(t2, s)
                val (e1, s2) = dCTriv(t1, s1)
                (parse(s"(A ${e1.show} ${e2.show})"), s2)
            }
        }
        def dCCont(c: Expression, es: (Expression, List[Expression])): Expression = {
            c match {
                case ListExp(StrExp("S"), StrExp(k)) if k.charAt(0) == '_' => es._1
                case ListExp(StrExp("F"), StrExp(n), e) if n.charAt(0) == '_' => dCExpr(e, es._1::es._2)
            }
        }
        e match {
            case ListExp(StrExp("F"), StrExp(k), f) if k.charAt(0) == '_' =>  dCExpr(f, List())
            case _ => e
        }
    }
}