package fast

import scala.collection.immutable.Map
import cats._, implicits._

class Lazy[A](val value: () => A)

object Lazy {
    def apply[A](value: => A): Lazy[A] = new Lazy(() => value)
    implicit object LazyMonad extends Bimonad[Lazy] {
        def pure[A](x: A): Lazy[A] = Lazy(x)
        def flatMap[A, B](fa: Lazy[A])(f: A => Lazy[B]): Lazy[B] = Lazy(f(fa.value()).value())
        def tailRecM[A, B](a: A)(f: A => Lazy[Either[A,B]]): Lazy[B] = flatMap(f(a))(_.fold(tailRecM(_)(f), pure(_)))
        def extract[A](x: Lazy[A]): A = x.value()
        def coflatMap[A, B](fa: Lazy[A])(f: Lazy[A] => B): Lazy[B] = Lazy(f(fa))
    }
}

class Eager[A](val value: () => A)

object Eager {
    def apply[A](value: A): Eager[A] = new Eager(() => value)
    implicit object EagerMonad extends Bimonad[Eager] {
        def pure[A](x: A): Eager[A] = Eager(x)
        def flatMap[A, B](fa: Eager[A])(f: A => Eager[B]): Eager[B] = Eager(f(fa.value()).value())
        def tailRecM[A, B](a: A)(f: A => Eager[Either[A,B]]): Eager[B] = flatMap(f(a))(_.fold(tailRecM(_)(f), pure(_)))
        def extract[A](x: Eager[A]): A = x.value()
        def coflatMap[A, B](fa: Eager[A])(f: Eager[A] => B): Eager[B] = Eager(f(fa))
    }
}

case class Lambda[F[_]](x: Int, y: Int, v: Map[Int, F[Either[Lambda[F], Lambda[F]]]], toExpression: () => F[Expression])

object Interpreter {
    def execute[F[_]: Bimonad](op: Array[Operation]): Expression = {
        def execute(pos: Int)(v: Map[Int, F[Either[Lambda[F], Lambda[F]]]]): F[Either[Lambda[F], Lambda[F]]] = {
            op(pos) match {
                case F(x,y) => {
                    val toExpression = () => execute(y)(v-x).flatMap[Expression](_.fold(
                        z => z.toExpression().map(ListExp(StrExp("F"), StrExp(Operation.itos(x)), _)),
                        z => z.toExpression().map(ListExp(StrExp("F"), StrExp(Operation.itos(x)),_))
                    ))
                    Monad[F].pure(Right(Lambda(x, y, v, toExpression)))
                }
                case A(x,y) => execute(x)(v).flatMap(_.fold(
                    z => execute(y)(v).map(_.fold(
                        w => Left(Lambda(-1, -1, z.v, () => (z.toExpression(), w.toExpression()).mapN(ListExp(StrExp("A"), _, _)))),
                        w => Left(Lambda(-1, -1, z.v, () => (z.toExpression(), w.toExpression()).mapN(ListExp(StrExp("A"), _, _))))
                    )),
                    z => execute(z.y)(z.v+((z.x, execute(y)(v))))
                ))
                case S(x,_) => {
                    val toExpression = () => Monad[F].pure[Expression](ListExp(StrExp("S"), StrExp(Operation.itos(x))))
                    v.getOrElse(x, Monad[F].pure(Left(Lambda(-1, -1, v, toExpression))))
                }
                case T(x, _) => execute(x)(v)
                case _ => throw new Error("Invalid Operation")
            }
        }
        execute(op.indexWhere(_.isInstanceOf[T]))(Map.empty).flatMap(_.fold(_.toExpression(), _.toExpression())).extract
    }
}