package fast

import scala.util.chaining._
import java.nio.ByteBuffer
import cats._, implicits._

sealed trait Operation
final case class F(x: Int, y: Int) extends Operation
final case class S(x: Int, y: Int) extends Operation
final case class A(x: Int, y: Int) extends Operation
final case class T(x: Int, y: Int) extends Operation

object Operation {
  def stoi(x: String): Int = {
    (x + 0.toChar.toString * Integer.BYTES)
      .slice(0, Integer.BYTES)
      .getBytes
      .pipe(ByteBuffer.wrap(_).getInt)
  }
  def itos(x: Int): String = {
    ByteBuffer
      .allocate(Integer.BYTES)
      .putInt(x)
      .array
      .filter(_ != 0)
      .map(_.toChar)
      .mkString
  }
  implicit object OrationShow extends Show[Operation] {
    def show(t: Operation): String = {
      t match {
        case A(x, y) => "A %08x %08x".format(x, y)
        case F(x, y) => "F %08x %08x".format(x, y)
        case S(x, y) => "S %08x %08x".format(x, y)
        case T(x, y) => "T %08x %08x".format(x, y)
      }
    }
  }
  implicit object OperationShow extends Show[Operation] {
    def show(t: Operation): String = {
      t match {
        case A(x, y) => "A %08x %08x".format(x, y)
        case F(x, y) => "F %08x %08x".format(x, y)
        case S(x, y) => "S %08x %08x".format(x, y)
        case T(x, y) => "A %08x %08x".format(x, y)
      }
    }
  }
}
