package fast

import atto._, Atto._

object ExpressionParser {
  val symbol: Parser[Expression] =
    many1(charRange('_' to '_', 'a' to 'z', 'A' to 'Z')).map { x =>
      StrExp(x.toList.mkString)
    }
  val list: Parser[Expression] =
    parens(sepBy1(symbol | list, many1(whitespace))).map { x =>
      ListExp(x.toList.toArray: _*)
    }
  val expression = bracket(many(whitespace), list | symbol, many(whitespace))
}
