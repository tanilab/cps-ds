package fast
import cats._, implicits._
import atto._, Atto._

object Main extends App {
  val code =
    "(A (F f (A (F x (A (S f) (A (S x) (S x)))) (F x (A (S f) (A (S x) (S x)))))) (F x (S y)))"
  ExpressionParser.expression
    .parseOnly(code)
    .map(Transformer.cpsTransform)
    .map(Transformer.dsTransform)
    .map(Compiler.compile)
    .map(Interpreter.execute[Lazy])
    .map(x => { println(x.show) })
}
