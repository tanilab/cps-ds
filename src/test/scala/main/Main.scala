package fast

import org.scalatest._

class MainSpec extends FlatSpec with Matchers {
  "The Hello object" should "say hello" in {
    "Hello" shouldEqual "hello"
  }
}
